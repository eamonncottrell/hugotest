---
title: About me
subtitle: A brief synopsis
comments: false
---

My name is Eamonn. I have the following qualities:

- I can endure long distance running and enjoy doing it
- I have created and attempt to maintain multiple podcasts
- I have three awesome kids and a killer wife
- I love John Irving novels and the music of Tool

What else do you need?

### my history

You may simply go here for some aural enjoyment of my creation: [Undertow Podcasts](https://eamonncottrell.com).