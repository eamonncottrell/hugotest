---
title: "Heat(around(the(corner)))"
date: 2020-10-30T20:36:49-04:00
draft: false
---

When are things good enough? 

It really matters a lot what the things are we’re talking about.

When I married my wife, we were both very sure that our relationship was good enough to imagine, albeit at least at that moment in time, marriage and life together and kids and all that. Sure, that decision differs depending on who you ask, but generally for many and definitely for us, it was a huge choice and it mattered a lot to both of us to get it right.

Some software should be a well committed engagement with a large degree of certainty that it not just works but that it is the best imaginable solution at that moment in time. Sure, vows can be renewed and strengthened later, and yes, sometimes time and change can erode that code if it is not well cared for, but on the whole this software is good, efficient and well loved.

In other circumstances, and I’ve been encountering these in the problem sets of CS50 this week, the code should work. And, it’s good practice to make sure it works well and that I’m grasping the concepts behind it. However, it doesn’t need to be perfect. 

<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/6c8eea56"></iframe>

More important is the progression and forward progress that I am making. 

Time is finite, and particularly as I’ve been wading through the infinite waters of self-taught software development, I believe that the momentum I create and maintain now is of more importance than the perfection of what my code could be were I to spend more time on it.

[The action is the juice!](https://youtu.be/LUy2Wx_r0_w) I gotta get building and put these tenets of code into practice!

There isn’t enough time for everything I’d like to do. There will never be enough time, even if I had double what is available now. Efficiency and progress then are the cornerstones on which I ought to build.
