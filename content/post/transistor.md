---
title: "Podcasting Host"
subtitle: "Transistor ftw"
date: 2020-07-31T14:40:55-04:00
# publishdate:
# draft: false
# expiredate:
author: Eamonn Cottrell
---

# Greetings
So, I use Transistor and they're wonderful.

<a href="https://transistor.fm/?via=eamonn">This here is an affiliate link</a>, but please do check them out. I've been with old and new podcasting companies since 2006 when I first started my adventures in audio, and Transistor has a wonderful combination of nailing the basics with a great user experience as well as giving some nice perks like unlimited show creations <<--this is what made me sign up with them. 